frame = {
    ctx : null,
    canvas : null,
    bodyPartTypes : [
        "head", "neck", "chest",
        "leftArm", "rightArm", "leftLeg", "rightLeg"
    ],
    body : {},
    prevBody: null,
    bodyPartCount : 0,
    reset : () => {
        frame.body={
            head : {},
            neck : {},
            chest: {},
            leftArm: {},
            rightArm: {},
            leftLeg: {},
            rightLeg: {}
        }
        frame.bodyPartCount = 0;
        frame.ctx.clearRect(0, 0, frame.canvas.width, frame.canvas.height);
    },
    getCurrentBodyPartName : () => {
        return frame.bodyPartTypes[frame.bodyPartCount];
    },
    hasNextBodyPart : () => {
        return !(frame.bodyPartCount == frame.bodyPartTypes.length);
    },
    nextBodyPart : (mouseX, mouseY) => {                
        let t = frame.bodyPartTypes[frame.bodyPartCount];                
        frame.body[t].x=mouseX-20;
        frame.body[t].y=mouseY-20;                                        
        frame.bodyPartCount++
    },  
    drawHead : (x,y) => {
        frame.ctx.beginPath();
        frame.ctx.arc(x,y,20,0,2*Math.PI);
        frame.ctx.lineWidth=10;
        frame.ctx.fillStyle = 'white';
        frame.ctx.fill();
        frame.ctx.stroke();
    },
    drawStick : (startX, startY, endX, endY) => {
        frame.ctx.beginPath();
        frame.ctx.moveTo(startX,startY);
        frame.ctx.lineTo(endX, endY);
        frame.ctx.lineWidth=10;
        frame.ctx.stroke();
    },
    redraw : (mouseX, mouseY) => {         
        frame.ctx.clearRect(0, 0, frame.canvas.width, frame.canvas.height);                
        let t = frame.bodyPartTypes[frame.bodyPartCount];        
        if (frame.prevBody != null) {
            draw.color = 'gray';            
            draw.drawBody(frame.prevBody, 0, 0);            
        }
        switch(t) {
            case 'head':
                draw.head(mouseX-20, mouseY-20)
                break;
            case 'neck':
                draw.stick(frame.body.head.x, frame.body.head.y, mouseX-20, mouseY-20)
                break;
            case 'chest':
                draw.stick(frame.body.neck.x, frame.body.neck.y, mouseX-20, mouseY-20)
                break;
            case 'leftArm':
                draw.stick(frame.body.neck.x, frame.body.neck.y, mouseX-20, mouseY-20)
                break;
            case 'rightArm':
                draw.stick(frame.body.neck.x, frame.body.neck.y, mouseX-20, mouseY-20)
                break;
            case 'leftLeg':
                draw.stick(frame.body.chest.x, frame.body.chest.y, mouseX-20, mouseY-20)
                break;
            case 'rightLeg':
                draw.stick(frame.body.chest.x, frame.body.chest.y, mouseX-20, mouseY-20)
                break;
            default:
                null;
        }        
        draw.color = 'black';
        draw.drawBody(frame.body, 0, 0);                        
    }
}
