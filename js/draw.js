draw = {        
    color : 'black',
    ctx : null,
    smScale: 0.15,
    screenScale : 1,
    baseTileImg : null,
    ladderTileImg : null,
    init (baseTileImg, ladderTileImg){
        draw.baseTileImg = baseTileImg;
        draw.ladderTileImg = ladderTileImg;
    },
    head : (x,y) => {
        if (draw.ctx == null) return;
        let s = screen.zoom * draw.smScale;        
        draw.ctx.strokeStyle = draw.color;
        draw.ctx.beginPath();
        draw.ctx.arc(
            x,
            y,
            20*s,
            0,
            2*Math.PI
        ); 
        draw.ctx.lineWidth=8*s;
        draw.ctx.fillStyle = draw.color;
        draw.ctx.fill();
        draw.ctx.stroke();
    },
    stick : (startX, startY, endX, endY) => {
        if (draw.ctx == null) return;
        let s = screen.zoom * draw.smScale;       
        draw.ctx.strokeStyle = draw.color;
        draw.ctx.beginPath();        
        draw.ctx.moveTo(startX,startY);
        draw.ctx.lineTo(endX, endY);
        draw.ctx.lineWidth=8*s;
        draw.ctx.stroke();
    },
    block : (x, y, size, color) => {
        if (draw.ctx == null) return;
        draw.ctx.fillStyle = color;        
        draw.ctx.fillRect(x,y,size,size);        
    },
    rect : (x, y, size, color) => {
        if (draw.ctx == null) return;
        draw.ctx.lineWidth = 2 * screen.zoom;
        draw.ctx.beginPath();
        draw.ctx.strokeStyle = color;
        draw.ctx.rect(x,y,size,size);        
        draw.ctx.stroke();
    },
    drawBody : (body, x = 0, y = 0) => {        
        if (draw.ctx == null) return;
        let s = screen.zoom * draw.smScale;        
        draw.stick(body.chest.x*s+x, body.chest.y*s+y, body.leftLeg.x*s+x, body.leftLeg.y*s+y) 
        draw.stick(body.chest.x*s+x, body.chest.y*s+y, body.rightLeg.x*s+x, body.rightLeg.y*s+y)
        draw.stick(body.neck.x*s+x, body.neck.y*s+y, body.leftArm.x*s+x, body.leftArm.y*s+y) 
        draw.stick(body.neck.x*s+x, body.neck.y*s+y, body.rightArm.x*s+x, body.rightArm.y*s+y)
        draw.stick(body.neck.x*s+x, body.neck.y*s+y, body.chest.x*s+x, body.chest.y*s+y)
        draw.stick(body.head.x*s+x, body.head.y*s+y, body.neck.x*s+x, body.neck.y*s+y)
        draw.head(body.head.x*s+x, body.head.y*s+y);
    },
    drawHealthBar : (health, maxHealth, x = 0, y = 0) => {    
        if (draw.ctx == null) return;
        if (health < 0) health = 0;
        let s = screen.zoom; 
        draw.ctx.lineWidth = 1 * screen.zoom;
        draw.ctx.beginPath();        
        draw.ctx.strokeStyle = 'red';
        draw.ctx.fillRect(x+9,y-20,health/maxHealth*70,7)
        draw.ctx.strokeStyle = '#333';
        draw.ctx.rect(x+9,y-20,70,7)
        draw.ctx.stroke();
    }, 
    drawMap : (map, blockSize, offsetX, offsetY) => {
        if (draw.ctx == null) return;
        if (map.length < 0) return;
        for (var i=0; i<map.length; i++){            
            for (var j=0; j<map[0].length; j++){
                // todo: optimize. if tile out of bounds, dont draw!
                let block = map[i][j];
                if (block > 110) block-=110;
                if (block == 1) {                    
                    draw.ctx.drawImage(draw.baseTileImg, i*blockSize-offsetX, j*blockSize-offsetY, blockSize, blockSize);
                } else if (block == 2) {        
                    draw.ctx.drawImage(draw.ladderTileImg, i*blockSize-offsetX, j*blockSize-offsetY, blockSize, blockSize);
                } /*else if (block >= 110) {                    
                    draw.block(i*blockSize, j*blockSize, blockSize, '#d9ffc6', draw.ctx)                    
                }*/
            }
        }
    },
    clear : (w, h) => {
        if (draw.ctx == null) return;
        draw.ctx.clearRect(0, 0, w, h); 
    }
}
