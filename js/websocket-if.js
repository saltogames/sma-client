/* 
 * Websocket interface
 * @author gszemes@saltostudio.hu
 */

wsif = {
    
     url : "http://localhost:8080/sma/",
    //url : "http://192.168.20.130:8080/sma/",
    // url : "http://10.0.1.1:8080/sma/",
    // url : "http://192.168.20.159:8080/sma/",    
    
    /** To be defined from outseide*/
    disconnected : function(){},    
    loggedIn : function(msg){},
    /***/
        
    playerId : 0,        
    stompClient : null,
    connect : function(connectedCallback, loginCallback, scheduledBroadcastRecieved){
        var socket = new SockJS(wsif.url);    
        wsif.stompClient = Stomp.over(socket);
        wsif.stompClient.debug = null;
        console.log("<WSIF> Connecting")
        wsif.stompClient.connect({}, function (/*frame*/) {            
            // console.log('Connected'/* + frame*/);     
            console.log("<WSIF> Connected")
            connectedCallback();
            wsif.stompClient.subscribe('/broadcast/characters', function (msg) {   
                // console.log("<WSIF> Scheduled data recieved")
                scheduledBroadcastRecieved(JSON.parse(msg.body));                
            });
            wsif.stompClient.subscribe('/broadcast/loggedin', function (msg) {                       
                let data = JSON.parse(msg.body);
                if (data.player.id == wsif.playerId){
                    console.log("<WSIF> Logged in as anonymus")
                    loginCallback(data.player, data.map);
                }
            });  
        });
        
    },
    disconnect : function() {
        if (this.stompClient != null) {
            this.stompClient.disconnect();
        }
        wsif.disconnected();
        console.log("Disconnected");        
    },
    anonimLogin : function(){    
        console.log("<WSIF> Login request...")
        let uid = new Date().getTime();
        wsif.playerId = uid;
        wsif.stompClient.send(
            "/app/anonimlogin",
            {},
            uid            
        );
    },
    sendKeyDown : function(keyCode){                            
        wsif.stompClient.send(
            "/app/keydown",
            {},
            JSON.stringify({
                uid: player.id,
                keyCode: keyCode
            })
        );
    },
    sendKeyUp : function(keyCode){                            
        wsif.stompClient.send(
            "/app/keyup",
            {},
            JSON.stringify({
                uid: player.id,
                keyCode: keyCode
            })
        );
    },
}